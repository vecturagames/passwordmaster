package com.vecturagames.android.app.passwordmaster.enumeration

enum class AppThemeType {

    LIGHT, DARK

}
