package com.vecturagames.android.app.passwordmaster.preference

import android.content.SharedPreferences
import android.util.TypedValue
import android.view.ContextThemeWrapper

import androidx.core.content.ContextCompat
import androidx.preference.PreferenceManager

import com.vecturagames.android.app.passwordmaster.MainApplication
import com.vecturagames.android.app.passwordmaster.R
import com.vecturagames.android.app.passwordmaster.enumeration.AppThemeType
import com.vecturagames.android.app.passwordmaster.util.Util

import de.slackspace.openkeepass.KeePassDatabase
import de.slackspace.openkeepass.domain.*
import de.slackspace.openkeepass.exception.KeePassDatabaseUnreadableException

import java.io.File
import java.io.FileOutputStream
import java.util.*

class AppSettings private constructor() {

    var mRateDialogShowed: Boolean = false
    var mApplicationLaunchCount: Int = 0
    var mApplicationLastVersionCode: Int = 0

    var mAppTheme: AppThemeType = AppThemeType.DARK
    var mAutoClipboardClearing: Boolean = true
    var mPasswordUseLowerCase: Boolean = false
    var mPasswordUseUpperCase: Boolean = false
    var mPasswordUseNumbers: Boolean = false
    var mPasswordUseSymbols: Boolean = false
    var mPasswordSymbols: String? = null
    var mPasswordUseUniqueChars: Boolean = false
    var mPasswordUseSimilarChars: Boolean = false
    var mPasswordSeed: String? = null
    var mPasswordLength: Int = 0

    var mPasswordDb: KeePassFile? = null
    var mPasswordDbMasterPassword: String? = null
    var mPasswordDbCurrentGroupName: String? = null

    private var mPreferences: SharedPreferences? = null

    val appThemeId: Int
        get() = if (mAppTheme === AppThemeType.LIGHT) {
            R.style.AppBaseTheme
        }
        else {
            R.style.AppBaseThemeDark
        }

    init {
        mPreferences = PreferenceManager.getDefaultSharedPreferences(MainApplication.appContext!!)

        setDefaultNonResetableSettings()
        setDefaultSettings()

        loadSettings()
    }

    private fun setDefaultNonResetableSettings() {
        mRateDialogShowed = false
        mApplicationLaunchCount = 0
        mApplicationLastVersionCode = 0
        mPasswordDb = null
        mPasswordDbMasterPassword = null
        mPasswordDbCurrentGroupName = null
    }

    private fun setDefaultSettings() {
        mAppTheme = AppThemeType.DARK
        mAutoClipboardClearing = true
        mPasswordUseLowerCase = true
        mPasswordUseUpperCase = true
        mPasswordUseNumbers = true
        mPasswordUseSymbols = false
        mPasswordSymbols = DEFAULT_PASSWORD_SYMBOLS
        mPasswordUseUniqueChars = false
        mPasswordUseSimilarChars = true
        mPasswordSeed = ""
        mPasswordLength = DEFAULT_PASSWORD_LENGTH
    }

    fun getColor(id: Int): Int {
        var id = id
        val context = MainApplication.appContext
        val th = ContextThemeWrapper(context, appThemeId)

        val typedValue = TypedValue()
        th.theme.resolveAttribute(id, typedValue, true)

        if (typedValue.resourceId != 0) {
            id = typedValue.resourceId
        }

        return ContextCompat.getColor(context!!, id)
    }

    fun unlockPasswordDb(contextThemeWrapper: ContextThemeWrapper, password: String, forceUnlock: Boolean = false): KeePassFile? {
        if (mPasswordDb == null || forceUnlock) {
            if (passwordDbExists(contextThemeWrapper)) {
                try {
                    mPasswordDb = KeePassDatabase.getInstance(getPasswordDbFilePath(contextThemeWrapper)).openDatabase(password)
                    mPasswordDbMasterPassword = password
                }
                catch (exception: KeePassDatabaseUnreadableException) {
                    return null
                }
            }
            else {
                return null
            }
        }

        return mPasswordDb
    }

    fun writePasswordDb(contextThemeWrapper: ContextThemeWrapper) {
        if (mPasswordDb != null && mPasswordDbMasterPassword != null) {
            KeePassDatabase.write(mPasswordDb, mPasswordDbMasterPassword, FileOutputStream(getPasswordDbFilePath(contextThemeWrapper)))
            mPasswordDb = unlockPasswordDb(contextThemeWrapper, mPasswordDbMasterPassword!!, true)
        }
    }

    fun getPasswordDbCurrentGroup(): Group? {
        if (mPasswordDb != null && mPasswordDbCurrentGroupName != null) {
            return mPasswordDb!!.topGroups.find { it.name.lowercase(Locale.ENGLISH) == mPasswordDbCurrentGroupName!!.lowercase(Locale.ENGLISH) }
        }
        return null
    }

    fun saveSettings() {
        val editor = mPreferences!!.edit()

        editor.putBoolean(SETTINGS_RATE_DIALOG_SHOWED, mRateDialogShowed)
        editor.putInt(SETTINGS_APPLICATION_LAUNCH_COUNT, mApplicationLaunchCount)
        editor.putInt(SETTINGS_APPLICATION_LAST_VERSION_CODE, Util.getVersionCode(MainApplication.appContext!!))
        editor.putInt(SETTINGS_APP_THEME, mAppTheme.ordinal)
        editor.putBoolean(SETTINGS_AUTO_CLIPBOARD_CLEARING, mAutoClipboardClearing)
        editor.putBoolean(SETTINGS_PASSWORD_USE_LOWER_CASE, mPasswordUseLowerCase)
        editor.putBoolean(SETTINGS_PASSWORD_USE_UPPER_CASE, mPasswordUseUpperCase)
        editor.putBoolean(SETTINGS_PASSWORD_USE_NUMBERS, mPasswordUseNumbers)
        editor.putBoolean(SETTINGS_PASSWORD_USE_SYMBOLS, mPasswordUseSymbols)
        editor.putString(SETTINGS_PASSWORD_SYMBOLS, mPasswordSymbols)
        editor.putBoolean(SETTINGS_PASSWORD_USE_UNIQUE_CHARS, mPasswordUseUniqueChars)
        editor.putBoolean(SETTINGS_PASSWORD_USE_SIMILAR_CHARS, mPasswordUseSimilarChars)
        editor.putString(SETTINGS_PASSWORD_SEED, mPasswordSeed)
        editor.putInt(SETTINGS_PASSWORD_LENGTH, mPasswordLength)

        editor.apply()
    }

    private fun loadSettings() {
        fixOldSettings()

        mRateDialogShowed = mPreferences!!.getBoolean(SETTINGS_RATE_DIALOG_SHOWED, mRateDialogShowed)
        mApplicationLaunchCount = mPreferences!!.getInt(SETTINGS_APPLICATION_LAUNCH_COUNT, mApplicationLaunchCount)
        mApplicationLastVersionCode = mPreferences!!.getInt(SETTINGS_APPLICATION_LAST_VERSION_CODE, mApplicationLastVersionCode)
        mAppTheme = AppThemeType.values()[mPreferences!!.getInt(SETTINGS_APP_THEME, mAppTheme.ordinal)]
        mAutoClipboardClearing = mPreferences!!.getBoolean(SETTINGS_AUTO_CLIPBOARD_CLEARING, mAutoClipboardClearing)
        mPasswordUseLowerCase = mPreferences!!.getBoolean(SETTINGS_PASSWORD_USE_LOWER_CASE, mPasswordUseLowerCase)
        mPasswordUseUpperCase = mPreferences!!.getBoolean(SETTINGS_PASSWORD_USE_UPPER_CASE, mPasswordUseUpperCase)
        mPasswordUseNumbers = mPreferences!!.getBoolean(SETTINGS_PASSWORD_USE_NUMBERS, mPasswordUseNumbers)
        mPasswordUseSymbols = mPreferences!!.getBoolean(SETTINGS_PASSWORD_USE_SYMBOLS, mPasswordUseSymbols)
        mPasswordSymbols = mPreferences!!.getString(SETTINGS_PASSWORD_SYMBOLS, mPasswordSymbols)
        mPasswordUseUniqueChars = mPreferences!!.getBoolean(SETTINGS_PASSWORD_USE_UNIQUE_CHARS, mPasswordUseUniqueChars)
        mPasswordUseSimilarChars = mPreferences!!.getBoolean(SETTINGS_PASSWORD_USE_SIMILAR_CHARS, mPasswordUseSimilarChars)
        mPasswordSeed = mPreferences!!.getString(SETTINGS_PASSWORD_SEED, mPasswordSeed)
        mPasswordLength = mPreferences!!.getInt(SETTINGS_PASSWORD_LENGTH, mPasswordLength)
    }

    private fun fixOldSettings() {
        val editor = mPreferences!!.edit()

        // Update of old version settings
        if (mPreferences!!.contains("SETTINGS_PASSWORD_LOWER_CASE")) {
            editor.putBoolean(SETTINGS_PASSWORD_USE_LOWER_CASE, mPreferences!!.getBoolean("SETTINGS_PASSWORD_LOWER_CASE", mPasswordUseLowerCase))
            editor.remove("SETTINGS_PASSWORD_LOWER_CASE")
        }
        if (mPreferences!!.contains("SETTINGS_PASSWORD_UPPER_CASE")) {
            editor.putBoolean(SETTINGS_PASSWORD_USE_UPPER_CASE, mPreferences!!.getBoolean("SETTINGS_PASSWORD_UPPER_CASE", mPasswordUseLowerCase))
            editor.remove("SETTINGS_PASSWORD_UPPER_CASE")
        }
        if (mPreferences!!.contains("SETTINGS_PASSWORD_NUMBERS")) {
            editor.putBoolean(SETTINGS_PASSWORD_USE_NUMBERS, mPreferences!!.getBoolean("SETTINGS_PASSWORD_NUMBERS", mPasswordUseLowerCase))
            editor.remove("SETTINGS_PASSWORD_NUMBERS")
        }
        if (mPreferences!!.contains("SETTINGS_PASSWORD_UNIQUE_CHARS")) {
            editor.putBoolean(SETTINGS_PASSWORD_USE_UNIQUE_CHARS, mPreferences!!.getBoolean("SETTINGS_PASSWORD_UNIQUE_CHARS", mPasswordUseLowerCase))
            editor.remove("SETTINGS_PASSWORD_UNIQUE_CHARS")
        }

        editor.apply()
    }

    companion object {

        val MIN_PASSWORD_LENGTH = 1
        val MAX_PASSWORD_LENGTH = 999
        val DEFAULT_PASSWORD_LENGTH = 20
        val DEFAULT_PASSWORD_SYMBOLS = "\'`\"!?,.:;$%&@~#()<>{}[]_*-+^=/|\\"

        val PASSWORD_LOWER_CASE = "abcdefghijklmnopqrstuvwxyz"
        val PASSWORD_UPPER_CASE = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        val PASSWORD_NUMBERS = "0123456789"

        val SIMILAR_CHARS = arrayOf("0O", "1lI")

        val CLIPBOARD_CLEAR_DELAY = 15000L
        val CLIPBOARD_LABEL = "clip"

        private val SETTINGS_RATE_DIALOG_SHOWED = "SETTINGS_RATE_DIALOG_SHOWED"
        private val SETTINGS_APPLICATION_LAUNCH_COUNT = "SETTINGS_APPLICATION_LAUNCH_COUNT"
        private val SETTINGS_APPLICATION_LAST_VERSION_CODE = "SETTINGS_APPLICATION_LAST_VERSION_CODE"

        private val SETTINGS_APP_THEME = "SETTINGS_APP_THEME"
        private val SETTINGS_AUTO_CLIPBOARD_CLEARING = "SETTINGS_AUTO_CLIPBOARD_CLEARING"
        private val SETTINGS_PASSWORD_USE_LOWER_CASE = "SETTINGS_PASSWORD_USE_LOWER_CASE"
        private val SETTINGS_PASSWORD_USE_UPPER_CASE = "SETTINGS_PASSWORD_USE_UPPER_CASE"
        private val SETTINGS_PASSWORD_USE_NUMBERS = "SETTINGS_PASSWORD_USE_NUMBERS"
        private val SETTINGS_PASSWORD_USE_SYMBOLS = "SETTINGS_PASSWORD_USE_SYMBOLS"
        private val SETTINGS_PASSWORD_SYMBOLS = "SETTINGS_PASSWORD_SYMBOLS"
        private val SETTINGS_PASSWORD_USE_UNIQUE_CHARS = "SETTINGS_PASSWORD_USE_UNIQUE_CHARS"
        private val SETTINGS_PASSWORD_USE_SIMILAR_CHARS = "SETTINGS_PASSWORD_USE_SIMILAR_CHARS"
        private val SETTINGS_PASSWORD_SEED = "SETTINGS_PASSWORD_SEED"
        private val SETTINGS_PASSWORD_LENGTH = "SETTINGS_PASSWORD_LENGTH"

        val DEFAULT_ICONS = arrayOf(
            R.drawable.ic_key,
            R.drawable.ic_world,
            R.drawable.ic_warning,
            R.drawable.ic_device_hub,
            R.drawable.ic_event_note,
            R.drawable.ic_social,
            R.drawable.ic_widgets,
            R.drawable.ic_note,
            R.drawable.ic_power,
            R.drawable.ic_android,
            R.drawable.ic_favorite,
            R.drawable.ic_camera,
            R.drawable.ic_wifi_tethering,
            R.drawable.ic_keyboard,
            R.drawable.ic_power_settings,
            R.drawable.ic_scanner,
            R.drawable.ic_star_half,
            R.drawable.ic_album,
            R.drawable.ic_display,
            R.drawable.ic_email,
            R.drawable.ic_settings,
            R.drawable.ic_speaker_notes,
            R.drawable.ic_file,
            R.drawable.ic_wallpaper,
            R.drawable.ic_power_input,
            R.drawable.ic_contact_mail,
            R.drawable.ic_save,
            R.drawable.ic_cloud,
            R.drawable.ic_adjust,
            R.drawable.ic_all_inclusive,
            R.drawable.ic_call_to_action,
            R.drawable.ic_print,
            R.drawable.ic_image,
            R.drawable.ic_extension,
            R.drawable.ic_build,
            R.drawable.ic_laptop,
            R.drawable.ic_transform,
            R.drawable.ic_credit_card,
            R.drawable.ic_sync,
            R.drawable.ic_clock,
            R.drawable.ic_search,
            R.drawable.ic_attach,
            R.drawable.ic_developer_board,
            R.drawable.ic_trash,
            R.drawable.ic_bookmark,
            R.drawable.ic_close,
            R.drawable.ic_help,
            R.drawable.ic_work,
            R.drawable.ic_folder,
            R.drawable.ic_folder_open,
            R.drawable.ic_dice,
            R.drawable.ic_lock_open,
            R.drawable.ic_lock,
            R.drawable.ic_check,
            R.drawable.ic_edit,
            R.drawable.ic_photo_library,
            R.drawable.ic_book,
            R.drawable.ic_menu,
            R.drawable.ic_person,
            R.drawable.ic_fitness,
            R.drawable.ic_home,
            R.drawable.ic_star,
            R.drawable.ic_traffic,
            R.drawable.ic_texture,
            R.drawable.ic_flower,
            R.drawable.ic_info,
            R.drawable.ic_money,
            R.drawable.ic_assignment,
            R.drawable.ic_phone
        )

        private var mInstance: AppSettings? = null

        private fun createInstance(): AppSettings {
            mInstance = AppSettings()
            return mInstance as AppSettings
        }

        fun destroyInstance() {
            mInstance = null
        }

        val instance: AppSettings
            get() = if (mInstance != null) {
                mInstance!!
            }
            else {
                createInstance()
            }

        fun getPasswordDbFilePath(contextThemeWrapper: ContextThemeWrapper): String {
            return contextThemeWrapper.filesDir.absolutePath + File.separator + "db.kdbx"
        }

        fun passwordDbExists(contextThemeWrapper: ContextThemeWrapper): Boolean {
            return File(getPasswordDbFilePath(contextThemeWrapper)).exists()
        }

        fun createPasswordDb(contextThemeWrapper: ContextThemeWrapper, password: String): KeePassFile {
            val rootBuilder = GroupBuilder()

            val email: Group = GroupBuilder("Email").iconId(19).build()
            val eshop: Group = GroupBuilder("Eshop").iconId(64).build()
            val service: Group = GroupBuilder("Service").iconId(33).build()
            val social: Group = GroupBuilder("Social").iconId(5).build()
            val work: Group = GroupBuilder("Work").iconId(47).build()

            rootBuilder.addGroup(email)
            rootBuilder.addGroup(eshop)
            rootBuilder.addGroup(service)
            rootBuilder.addGroup(social)
            rootBuilder.addGroup(work)

            val passwordDb = KeePassFileBuilder("Passwords")
                .addTopGroups(rootBuilder.build())
                .build()

            KeePassDatabase.write(passwordDb, password, FileOutputStream(getPasswordDbFilePath(contextThemeWrapper)))

            return passwordDb
        }
    }

}
