package com.vecturagames.android.app.passwordmaster.activity

import android.content.res.Configuration
import android.os.Build
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Button
import android.widget.CheckBox
import android.widget.EditText
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.view.menu.MenuBuilder
import androidx.appcompat.widget.PopupMenu
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.DrawableCompat
import com.vecturagames.android.app.passwordmaster.R
import com.vecturagames.android.app.passwordmaster.preference.AppSettings
import com.vecturagames.android.app.passwordmaster.util.Dialog
import com.vecturagames.android.app.passwordmaster.util.Util
import de.slackspace.openkeepass.domain.Entry
import de.slackspace.openkeepass.domain.EntryBuilder
import de.slackspace.openkeepass.domain.Group
import java.security.SecureRandom
import java.util.Locale

class EntryActivity : AppCompatActivity() {

    private var mToolbar: Toolbar? = null

    private var mLinearLayoutEntry: LinearLayout? = null

    private var mCheckBoxLowerCase: CheckBox? = null
    private var mCheckBoxUpperCase: CheckBox? = null
    private var mCheckBoxNumbers: CheckBox? = null
    private var mCheckBoxSymbols: CheckBox? = null
    private var mCheckBoxUniqueChars: CheckBox? = null
    private var mCheckBoxSimilarChars: CheckBox? = null

    private var mTextViewStrength: TextView? = null

    private var mEditTextTitle: EditText? = null
    private var mEditTextURL: EditText? = null
    private var mEditTextUserName: EditText? = null
    private var mEditTextNotes: EditText? = null
    private var mEditTextPassword: EditText? = null
    private var mEditTextSymbols: EditText? = null
    private var mEditTextPasswordLength: EditText? = null
    private var mEditTextSeed: EditText? = null

    private var mProgressBarStrength: ProgressBar? = null

    private var mImageViewEntryIcon: ImageView? = null
    private var mImageViewTitleCopy: ImageView? = null
    private var mImageViewURLCopy: ImageView? = null
    private var mImageViewUserNameCopy: ImageView? = null
    private var mImageViewNotesCopy: ImageView? = null
    private var mImageViewPasswordCopy: ImageView? = null

    private var mButtonPasswordLengthMinus: Button? = null
    private var mButtonPasswordLengthPlus: Button? = null
    private var mButtonGenerate: Button? = null

    private val mRandomSecure = SecureRandom()

    private var mGroup: Group? = null
    private var mEntry: Entry? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setTheme(AppSettings.instance.appThemeId)
        setContentView(R.layout.activity_entry)

        mToolbar = findViewById(R.id.toolbar)
        setSupportActionBar(mToolbar)

        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        // LinearLayout
        mLinearLayoutEntry = findViewById(R.id.linearLayoutEntry);

        if (mLinearLayoutEntry != null && Build.VERSION.SDK_INT >= Build.VERSION_CODES.VANILLA_ICE_CREAM) {
            val placeholder: View = mLinearLayoutEntry!!.findViewById(R.id.android15statusBarPlaceHolder)
            Util.setStatusBarColor(placeholder, mLinearLayoutEntry!!, AppSettings.instance.getColor(R.attr.primary_dark))
        }

        // TextView
        mTextViewStrength = findViewById(R.id.textViewStrength)

        // ProgressBar
        mProgressBarStrength = findViewById(R.id.progressBarStrength)
        /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            mProgressBarStrength.setScaleY(1.5f);
        }*/

        // EditText
        mEditTextTitle = findViewById(R.id.editTextTitle)
        mEditTextURL = findViewById(R.id.editTextURL)
        mEditTextUserName = findViewById(R.id.editTextUserName)
        mEditTextNotes = findViewById(R.id.editTextNotes)
        mEditTextPassword = findViewById(R.id.editTextPassword)
        mEditTextSymbols = findViewById(R.id.editTextSymbols)
        mEditTextPasswordLength = findViewById(R.id.editTextPasswordLength)
        mEditTextSeed = findViewById(R.id.editTextSeed)

        mEditTextTitle!!.setText("")
        mEditTextURL!!.setText("")
        mEditTextUserName!!.setText("")
        mEditTextNotes!!.setText("")
        mEditTextPassword!!.setText("")

        mEditTextPassword!!.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(userEnteredValue: Editable) {

            }

            override fun onTextChanged(userEnteredValue: CharSequence, start: Int, before: Int, count: Int) {
                val passwordStrengths = resources.getStringArray(R.array.other_password_strength)

                val passwordEntropy = getPasswordEntropy(mEditTextPassword!!.text.toString())
                val passwordStrength: String
                val passwordStrengthIdx: Int
                val color: Int

                if (passwordEntropy >= 170) {
                    passwordStrengthIdx = 4
                    color = AppSettings.instance.getColor(R.attr.password_strength_ultra_strong)
                }
                else if (passwordEntropy >= 112) {
                    passwordStrengthIdx = 3
                    color = AppSettings.instance.getColor(R.attr.password_strength_very_strong)
                }
                else if (passwordEntropy >= 80) {
                    passwordStrengthIdx = 2
                    color = AppSettings.instance.getColor(R.attr.password_strength_strong)
                }
                else if (passwordEntropy >= 48) {
                    passwordStrengthIdx = 1
                    color = AppSettings.instance.getColor(R.attr.password_strength_medium)
                }
                else {
                    passwordStrengthIdx = 0
                    color = AppSettings.instance.getColor(R.attr.password_strength_weak)
                }

                passwordStrength = passwordStrengths[passwordStrengthIdx]

                mTextViewStrength!!.setTextColor(color)
                mTextViewStrength!!.text = getString(R.string.entry_activity_textview_strength, passwordStrength, passwordEntropy)
                mProgressBarStrength!!.progressDrawable.setColorFilter(color, android.graphics.PorterDuff.Mode.SRC_IN)
                mProgressBarStrength!!.progress = Math.min(Math.round(passwordEntropy * (100.0f / 112.0f)), 100)
            }

            override fun beforeTextChanged(userEnteredValue: CharSequence, start: Int, before: Int, count: Int) {

            }
        })

        if (mEditTextSymbols != null) {
            mEditTextSymbols!!.isEnabled = AppSettings.instance.mPasswordUseSymbols
            mEditTextSymbols!!.setText(AppSettings.instance.mPasswordSymbols)
            mEditTextSymbols!!.addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {

                }

                override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {

                }

                override fun afterTextChanged(editable: Editable) {
                    AppSettings.instance.mPasswordSymbols = editable.toString().trim { it <= ' ' }
                }
            })
        }

        if (mEditTextPasswordLength != null) {
            mEditTextPasswordLength!!.addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {

                }

                override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {

                }

                override fun afterTextChanged(editable: Editable) {
                    try {
                        AppSettings.instance.mPasswordLength = Util.clamp(Integer.valueOf(editable.toString()), AppSettings.MIN_PASSWORD_LENGTH, AppSettings.MAX_PASSWORD_LENGTH)
                    }
                    catch (e: NumberFormatException) {
                        AppSettings.instance.mPasswordLength = AppSettings.MIN_PASSWORD_LENGTH
                    }

                }
            })
            mEditTextPasswordLength!!.setText(AppSettings.instance.mPasswordLength.toString())
        }

        if (mEditTextSeed != null) {
            mEditTextSeed!!.setText(AppSettings.instance.mPasswordSeed)
            mEditTextSeed!!.addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {

                }

                override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {

                }

                override fun afterTextChanged(editable: Editable) {
                    AppSettings.instance.mPasswordSeed = editable.toString()
                }
            })
        }

        // CheckBox
        mCheckBoxLowerCase = findViewById(R.id.checkBoxLowerCase)
        mCheckBoxUpperCase = findViewById(R.id.checkBoxUpperCase)
        mCheckBoxNumbers = findViewById(R.id.checkBoxNumbers)
        mCheckBoxSymbols = findViewById(R.id.checkBoxCustomSymbols)
        mCheckBoxUniqueChars = findViewById(R.id.checkBoxUniqueChars)
        mCheckBoxSimilarChars = findViewById(R.id.checkBoxSimilarChars)

        if (mCheckBoxLowerCase != null) {
            mCheckBoxLowerCase!!.setOnCheckedChangeListener { button, checked -> AppSettings.instance.mPasswordUseLowerCase = checked }
            mCheckBoxLowerCase!!.isChecked = AppSettings.instance.mPasswordUseLowerCase
        }

        if (mCheckBoxUpperCase != null) {
            mCheckBoxUpperCase!!.setOnCheckedChangeListener { button, checked -> AppSettings.instance.mPasswordUseUpperCase = checked }
            mCheckBoxUpperCase!!.isChecked = AppSettings.instance.mPasswordUseUpperCase
        }

        if (mCheckBoxNumbers != null) {
            mCheckBoxNumbers!!.setOnCheckedChangeListener { button, checked -> AppSettings.instance.mPasswordUseNumbers = checked }
            mCheckBoxNumbers!!.isChecked = AppSettings.instance.mPasswordUseNumbers
        }

        if (mCheckBoxSymbols != null) {
            mCheckBoxSymbols!!.setOnCheckedChangeListener { button, checked ->
                AppSettings.instance.mPasswordUseSymbols = checked
                mEditTextSymbols!!.isEnabled = checked
            }
            mCheckBoxSymbols!!.isChecked = AppSettings.instance.mPasswordUseSymbols
        }

        if (mCheckBoxUniqueChars != null) {
            mCheckBoxUniqueChars!!.setOnCheckedChangeListener { button, checked -> AppSettings.instance.mPasswordUseUniqueChars = checked }
            mCheckBoxUniqueChars!!.isChecked = AppSettings.instance.mPasswordUseUniqueChars
        }

        if (mCheckBoxSimilarChars != null) {
            mCheckBoxSimilarChars!!.setOnCheckedChangeListener { button, checked -> AppSettings.instance.mPasswordUseSimilarChars = checked }
            mCheckBoxSimilarChars!!.isChecked = AppSettings.instance.mPasswordUseSimilarChars
        }

        // ImageView
        mImageViewEntryIcon = findViewById(R.id.imageViewEntryIcon)
        mImageViewTitleCopy = findViewById(R.id.imageViewTitleCopy)
        mImageViewURLCopy = findViewById(R.id.imageViewURLCopy)
        mImageViewUserNameCopy = findViewById(R.id.imageViewUserNameCopy)
        mImageViewNotesCopy = findViewById(R.id.imageViewNotesCopy)
        mImageViewPasswordCopy = findViewById(R.id.imageViewPasswordCopy)

        if (mImageViewEntryIcon != null) {
            val popupMenu = PopupMenu(this, mImageViewEntryIcon!!)
            val menu = popupMenu.menu

            var i = 0
            for (imageResId in AppSettings.DEFAULT_ICONS) {
                val drawable = ContextCompat.getDrawable(this, imageResId)
                DrawableCompat.setTint(drawable!!, AppSettings.instance.getColor(R.attr.primary_text))
                val menuItem: MenuItem = menu.add(Menu.NONE, i, i, "Icon " + (i + 1))
                menuItem.icon = drawable
                i++
            }

            popupMenu.setOnMenuItemClickListener { menuItem ->
                mImageViewEntryIcon!!.setImageDrawable(menuItem.icon)
                mImageViewEntryIcon!!.setTag(R.id.entry_image_icon_idx, menuItem.itemId)
                true
            }

            mImageViewEntryIcon!!.setOnClickListener {
                Util.showPopupMenuWithIcons(this@EntryActivity, popupMenu.menu as MenuBuilder, mImageViewEntryIcon!!)
            }
        }

        if (mImageViewTitleCopy != null) {
            mImageViewTitleCopy!!.setOnClickListener {
                if (!mEditTextTitle!!.text.toString().isEmpty()) {
                    Util.copyToClipboard(this, mEditTextTitle!!.text.toString())
                }
            }
        }

        if (mImageViewURLCopy != null) {
            mImageViewURLCopy!!.setOnClickListener {
                if (!mEditTextURL!!.text.toString().isEmpty()) {
                    Util.copyToClipboard(this, mEditTextURL!!.text.toString())
                }
            }
        }

        if (mImageViewUserNameCopy != null) {
            mImageViewUserNameCopy!!.setOnClickListener {
                if (!mEditTextUserName!!.text.toString().isEmpty()) {
                    Util.copyToClipboard(this, mEditTextUserName!!.text.toString())
                }
            }
        }

        if (mImageViewNotesCopy != null) {
            mImageViewNotesCopy!!.setOnClickListener {
                if (!mEditTextNotes!!.text.toString().isEmpty()) {
                    Util.copyToClipboard(this, mEditTextNotes!!.text.toString())
                }
            }
        }

        if (mImageViewPasswordCopy != null) {
            mImageViewPasswordCopy!!.setOnClickListener {
                if (!mEditTextPassword!!.text.toString().isEmpty()) {
                    Util.copyToClipboard(this, mEditTextPassword!!.text.toString())
                }
            }
        }

        // Button
        mButtonPasswordLengthMinus = findViewById(R.id.buttonPasswordLengthMinus)
        mButtonPasswordLengthPlus = findViewById(R.id.buttonPasswordLengthPlus)
        mButtonGenerate = findViewById(R.id.buttonGenerate)

        if (mButtonPasswordLengthMinus != null) {
            mButtonPasswordLengthMinus!!.setOnClickListener {
                if (AppSettings.instance.mPasswordLength > AppSettings.MIN_PASSWORD_LENGTH) {
                    mEditTextPasswordLength!!.setText((AppSettings.instance.mPasswordLength - 1).toString())
                }
            }
        }

        if (mButtonPasswordLengthPlus != null) {
            mButtonPasswordLengthPlus!!.setOnClickListener {
                if (AppSettings.instance.mPasswordLength < AppSettings.MAX_PASSWORD_LENGTH) {
                    mEditTextPasswordLength!!.setText((AppSettings.instance.mPasswordLength + 1).toString())
                }
            }
        }

        if (mButtonGenerate != null) {
            mButtonGenerate!!.setOnClickListener { generateAndShowPassword() }
        }

        updateCharactersText()

        if (intent.extras != null && AppSettings.instance.mPasswordDb != null) {
            val groupName = intent.extras!!.getString(BUNDLE_TAG_GROUP_NAME)
            val entryUUID = intent.extras!!.getString(BUNDLE_TAG_ENTRY_UUID)

            if (groupName != null && entryUUID != null) {
                mGroup = AppSettings.instance.mPasswordDb!!.topGroups.find { it.name.lowercase(Locale.ENGLISH) == groupName!!.lowercase(Locale.ENGLISH) }

                if (mGroup != null) {
                    mEntry = mGroup!!.entries.find { it.uuid.toString() == entryUUID }

                    if (mEntry != null) {
                        if (mImageViewEntryIcon != null) {
                            if (mEntry!!.iconId < AppSettings.DEFAULT_ICONS.size) {
                                val drawable = ContextCompat.getDrawable(this, AppSettings.DEFAULT_ICONS[mEntry!!.iconId])
                                mImageViewEntryIcon!!.setImageDrawable(drawable)
                                mImageViewEntryIcon!!.setTag(R.id.entry_image_icon_idx, mEntry!!.iconId)
                            }
                        }

                        if (mEditTextTitle != null) {
                            mEditTextTitle!!.setText(mEntry!!.title)
                        }

                        if (mEditTextURL != null) {
                            mEditTextURL!!.setText(mEntry!!.url)
                        }

                        if (mEditTextUserName != null) {
                            mEditTextUserName!!.setText(mEntry!!.username)
                        }

                        if (mEditTextNotes != null) {
                            mEditTextNotes!!.setText(mEntry!!.notes)
                        }

                        if (mEditTextPassword != null) {
                            mEditTextPassword!!.setText(mEntry!!.password)
                        }
                    }
                }
            }
        }
    }

    override fun onPause() {
        super.onPause()
        saveEntry()
        AppSettings.instance.saveSettings()
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        updateCharactersText()
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressedDispatcher.onBackPressed()
        return true
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.entry, menu)
        menu.findItem(R.id.auto_clipboard_clearing)?.setChecked(AppSettings.instance.mAutoClipboardClearing)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.auto_clipboard_clearing -> {
                AppSettings.instance.mAutoClipboardClearing = !AppSettings.instance.mAutoClipboardClearing
                item.setChecked(AppSettings.instance.mAutoClipboardClearing)
                return true
            }

            R.id.menu_reset_symbols -> {
                mEditTextSymbols!!.setText(AppSettings.DEFAULT_PASSWORD_SYMBOLS)
                return true
            }

            R.id.menu_tips -> {
                Dialog.showTipsDialog(this)
                return true
            }

            R.id.menu_password_strength -> {
                Dialog.showPasswordStrengthDialog(this)
                return true
            }

            R.id.menu_help -> {
                Dialog.showHelpDialog(this)
                return true
            }

            else -> return super.onOptionsItemSelected(item)
        }
    }

    private fun updateCharactersText() {
        val displayMetrics = Util.getDisplayMetrics(this)
        val size = Util.getDisplaySize(this)
        val widthDp = Util.pxToDp(displayMetrics, size.x)

        if (widthDp < 400) {
            if (mCheckBoxUniqueChars != null) {
                mCheckBoxUniqueChars!!.setText(R.string.entry_activity_chechbox_unique_chars)
            }

            if (mCheckBoxSimilarChars != null) {
                mCheckBoxSimilarChars!!.setText(R.string.entry_activity_chechbox_similar_chars)
            }
        }
        else {
            if (mCheckBoxUniqueChars != null) {
                mCheckBoxUniqueChars!!.setText(R.string.entry_activity_chechbox_unique_characters)
            }

            if (mCheckBoxSimilarChars != null) {
                mCheckBoxSimilarChars!!.setText(R.string.entry_activity_chechbox_similar_characters)
            }
        }
    }

    private fun getPasswordEntropy(password: String): Int {
        var n = 0
        var uniqueCharacters = 0

        for (i in 0 until password.length) {
            if (i == password.indexOf(password[i])) {
                uniqueCharacters++
            }
        }

        var hasLowerCase = false
        var hasUpperCase = false
        var hasNumbers = false
        var otherCharacters = ""

        for (i in 0 until password.length) {
            val character = password[i].toString()

            if (!hasLowerCase && AppSettings.PASSWORD_LOWER_CASE.contains(character)) {
                n += AppSettings.PASSWORD_LOWER_CASE.length
                hasLowerCase = true
            }
            else if (!hasUpperCase && AppSettings.PASSWORD_UPPER_CASE.contains(character)) {
                n += AppSettings.PASSWORD_UPPER_CASE.length
                hasUpperCase = true
            }
            else if (!hasNumbers && AppSettings.PASSWORD_NUMBERS.contains(character)) {
                n += AppSettings.PASSWORD_NUMBERS.length
                hasNumbers = true
            }
            else if (!otherCharacters.contains(character)) {
                n++
                otherCharacters += character
            }
        }

        val passwordEntropy = (uniqueCharacters * (Math.log10(n.toDouble()) / Math.log10(2.0))).toInt()
        return if (passwordEntropy == 0 && !password.isEmpty()) 1 else passwordEntropy
    }

    private fun generateAndShowPassword() {
        val password = generatePassword(AppSettings.instance.mPasswordLength, mEditTextSeed!!.text.toString())

        if (password.compareTo("") != 0) {
            mEditTextPassword!!.setText(password)
            mEditTextPassword!!.setSelection(mEditTextPassword!!.text.length)
        }
    }

    private fun generatePassword(length: Int, seed: String): String {
        var length = length
        val random = if (seed == "") mRandomSecure else SecureRandom(seed.toByteArray())

        var uniqueCharactersCount = 0

        var characters = ""
        if (mCheckBoxLowerCase!!.isChecked) {
            characters += AppSettings.PASSWORD_LOWER_CASE
            uniqueCharactersCount += AppSettings.PASSWORD_LOWER_CASE.length
        }
        if (mCheckBoxUpperCase!!.isChecked) {
            characters += AppSettings.PASSWORD_UPPER_CASE
            uniqueCharactersCount += AppSettings.PASSWORD_UPPER_CASE.length
        }
        if (mCheckBoxNumbers!!.isChecked) {
            characters += AppSettings.PASSWORD_NUMBERS
            uniqueCharactersCount += AppSettings.PASSWORD_NUMBERS.length
        }
        if (mCheckBoxSymbols!!.isChecked) {
            val symbols = mEditTextSymbols!!.text.toString().trim { it <= ' ' }
            var uniqueCharacters = characters

            for (i in 0 until symbols.length) {
                val symbol = symbols.get(i).toString()

                if (!uniqueCharacters.contains(symbol)) {
                    uniqueCharacters += symbol
                    uniqueCharactersCount++
                }
            }

            characters += symbols
        }

        if (!mCheckBoxSimilarChars!!.isChecked) {
            for (i in AppSettings.SIMILAR_CHARS.indices) {
                val currentSimilarChars = AppSettings.SIMILAR_CHARS[i]

                for (j in 0 until currentSimilarChars.length) {
                    val countOriginal = characters.length
                    characters = characters.replace(currentSimilarChars[j].toString().toRegex(), "")

                    if (countOriginal > characters.length) {
                        uniqueCharactersCount--
                    }
                }
            }
        }

        var password = ""

        if (characters != "") {
            length = Math.max(length, AppSettings.MIN_PASSWORD_LENGTH)

            for (i in 0 until length) {
                var rnd = random.nextInt(characters.length)
                var c = characters[rnd].toString()

                if (mCheckBoxUniqueChars!!.isChecked && password.length < uniqueCharactersCount) {
                    while (password.contains(c)) {
                        rnd = random.nextInt(characters.length)
                        c = characters[rnd].toString()
                    }
                }

                password += c
            }
        }

        return password
    }

    private fun saveEntry() {
        if (mGroup != null && mEntry != null) {
            val entry = EntryBuilder(mEntry)
                .title(mEditTextTitle?.text.toString())
                .url(mEditTextURL?.text.toString())
                .username(mEditTextUserName?.text.toString())
                .notes(mEditTextNotes?.text.toString())
                .password(mEditTextPassword?.text.toString())
                .iconId(mImageViewEntryIcon?.getTag(R.id.entry_image_icon_idx) as Int)
                .build()

            mGroup!!.entries.remove(mEntry)
            mGroup!!.entries.add(entry)
            mGroup!!.entries.sortBy { it.title.lowercase(Locale.ENGLISH) }

            AppSettings.instance.writePasswordDb(this)

            mEntry = entry
        }
    }

    companion object {
        val REQUEST_CODE_SHOW_ENTRY = 1000
        val BUNDLE_TAG_GROUP_NAME = "BUNDLE_TAG_GROUP_NAME"
        val BUNDLE_TAG_ENTRY_UUID = "BUNDLE_TAG_ENTRY_UUID"
    }

}
