package com.vecturagames.android.app.passwordmaster.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

import com.vecturagames.android.app.passwordmaster.R

class PasswordListFragment : Fragment() {

    private lateinit var mViewManager: RecyclerView.LayoutManager

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val root = inflater.inflate(R.layout.fragment_password_list, container, false)

        if (activity is RecyclerViewContract) {
            mViewManager = LinearLayoutManager(context)

            val recyclerView: RecyclerView = root.findViewById(R.id.recyclerView)
            recyclerView.apply {
                layoutManager = mViewManager
            }
            (activity as RecyclerViewContract).setRecyclerView(recyclerView)
        }

        return root
    }

}

interface RecyclerViewContract {
    fun setRecyclerView(recyclerView: RecyclerView)
}
