package com.vecturagames.android.app.passwordmaster.activity

import android.os.Build
import android.os.Bundle
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView

import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar

import com.vecturagames.android.app.passwordmaster.R
import com.vecturagames.android.app.passwordmaster.preference.AppSettings
import com.vecturagames.android.app.passwordmaster.util.Util

class AboutActivity : AppCompatActivity() {

    private var mToolbar: Toolbar? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setTheme(AppSettings.instance.appThemeId)
        setContentView(R.layout.activity_about)

        mToolbar = findViewById(R.id.toolbar)
        setSupportActionBar(mToolbar)

        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val linearLayoutAbout: LinearLayout = findViewById(R.id.linearLayoutAbout);

        if (linearLayoutAbout != null && Build.VERSION.SDK_INT >= Build.VERSION_CODES.VANILLA_ICE_CREAM) {
            val placeholder: View = linearLayoutAbout!!.findViewById(R.id.android15statusBarPlaceHolder)
            Util.setStatusBarColor(placeholder, linearLayoutAbout!!, AppSettings.instance.getColor(R.attr.primary_dark))
        }

        val textViewWebsiteValue = findViewById<View>(R.id.textViewWebsiteValue) as TextView
        textViewWebsiteValue.setOnClickListener { Util.openInternetBrowser(this@AboutActivity, getString(R.string.other_website), 0) }

        val textViewPrivacyPolicyValue = findViewById<View>(R.id.textViewPrivacyPolicyValue) as TextView
        textViewPrivacyPolicyValue.setOnClickListener { Util.openInternetBrowser(this@AboutActivity, getString(R.string.other_privacy_policy_website), 0) }

        val textViewEmailValue = findViewById<View>(R.id.textViewEmailValue) as TextView
        textViewEmailValue.setOnClickListener { Util.openEmailApplication(this@AboutActivity, getString(R.string.other_contact_email), 0) }

        val textViewSourceCodeValue = findViewById<View>(R.id.textViewSourceCodeValue) as TextView
        textViewSourceCodeValue.setOnClickListener { Util.openInternetBrowser(this@AboutActivity, getString(R.string.other_source_code_repository), 0) }

        val textViewVersionValue = findViewById<View>(R.id.textViewVersionValue) as TextView
        textViewVersionValue.text = Util.getVersionName(this)

        val textViewBuildNumberValue = findViewById<View>(R.id.textViewBuildNumberValue) as TextView
        val buildTime = Util.getBuildTime(this)
        val versionCode = Util.getVersionCode(this)

        if (versionCode > -1) {
            textViewBuildNumberValue.text = versionCode.toString() + if (buildTime != "") " ($buildTime)" else ""
        }
        else {
            textViewBuildNumberValue.text = getString(R.string.other_not_available)
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressedDispatcher.onBackPressed()
        return true
    }

}
