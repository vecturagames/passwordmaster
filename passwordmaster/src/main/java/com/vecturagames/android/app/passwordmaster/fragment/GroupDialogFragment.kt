package com.vecturagames.android.app.passwordmaster.fragment

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.EditText
import android.widget.ImageView

import androidx.appcompat.app.AlertDialog
import androidx.appcompat.view.menu.MenuBuilder
import androidx.appcompat.widget.PopupMenu
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.DrawableCompat
import androidx.fragment.app.DialogFragment

import com.vecturagames.android.app.passwordmaster.R
import com.vecturagames.android.app.passwordmaster.preference.AppSettings
import com.vecturagames.android.app.passwordmaster.util.Util

class GroupDialogFragment(groupName: String = "", imageResIdx: Int = 0) : DialogFragment() {

    private var mGroupDialogFragmentCallback: GroupDialogFragmentCallback? = null
    private var mGroupName: String
    private var mImageResIdx: Int

    init {
        mGroupName = groupName
        mImageResIdx = imageResIdx
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): AlertDialog {
        val builder = AlertDialog.Builder(requireActivity())

        val layout: View = requireActivity().layoutInflater.inflate(R.layout.fragment_group, null)

        val editTextGroupName: EditText = layout.findViewById(R.id.editTextGroupName)
        val imageViewGroupIcon: ImageView = layout.findViewById(R.id.imageViewGroupIcon)

        if (mImageResIdx < AppSettings.DEFAULT_ICONS.size) {
            imageViewGroupIcon.setImageDrawable(ContextCompat.getDrawable(requireActivity(), AppSettings.DEFAULT_ICONS[mImageResIdx]))
        }

        val popupMenu = PopupMenu(requireActivity(), imageViewGroupIcon)
        val menu = popupMenu.menu

        var imageResIdx = 0

        var i = 0
        for (imageResId in AppSettings.DEFAULT_ICONS) {
            val drawable = ContextCompat.getDrawable(requireActivity(), imageResId)
            DrawableCompat.setTint(drawable!!, AppSettings.instance.getColor(R.attr.primary_text))
            val menuItem: MenuItem = menu.add(Menu.NONE, i, i, "Icon " + (i+1))
            menuItem.icon = drawable
            i++
        }

        popupMenu.setOnMenuItemClickListener { menuItem ->
            imageViewGroupIcon.setImageDrawable(menuItem.icon)
            imageResIdx = menuItem.itemId
            true
        }

        imageViewGroupIcon.setOnClickListener {
            Util.showPopupMenuWithIcons(activity, popupMenu.menu as MenuBuilder, imageViewGroupIcon)
        }

        builder.setTitle(R.string.add_group_fragment_title)
        builder.setView(layout)

        builder.setPositiveButton(R.string.dialog_button_ok) { dialog, which ->
            if (mGroupDialogFragmentCallback != null) {
                mGroupDialogFragmentCallback!!.onGroupPositiveCallback(editTextGroupName.text.toString(), imageResIdx)
            }
        }

        return builder.create()
    }

    fun setAddGroupDialogFragmentCallback(groupDialogFragmentCallback: GroupDialogFragmentCallback) {
        mGroupDialogFragmentCallback = groupDialogFragmentCallback
    }

    companion object {
        fun newInstance(defaultGroupName: String, defaultImageResIdx: Int): GroupDialogFragment {
            return GroupDialogFragment(defaultGroupName, defaultImageResIdx)
        }
    }

}

interface GroupDialogFragmentCallback {
    fun onGroupPositiveCallback(groupName: String, imageResIdx: Int)
}
