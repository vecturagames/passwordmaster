package com.vecturagames.android.app.passwordmaster.adapter

import android.content.Context
import android.view.*
import android.widget.ImageView
import android.widget.TextView

import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView

import com.vecturagames.android.app.passwordmaster.R
import com.vecturagames.android.app.passwordmaster.preference.AppSettings

import de.slackspace.openkeepass.domain.Entry

class EntryAdapter(val items : List<Entry>, val context: Context, val onItemClickListener: EntryAdapterItemClickListener?, val onItemMenuClickListener: EntryAdapterItemMenuClickListener?) : RecyclerView.Adapter<ViewHolder>() {

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.layout_entry_item, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val entry = items.get(position)
        val drawable = ContextCompat.getDrawable(context, AppSettings.DEFAULT_ICONS[entry.iconId])

        holder.imageViewImage.setImageDrawable(drawable)
        holder.textViewTitle.text = if (entry.title.toString() != "") entry.title else context.getString(R.string.password_list_fragment_no_title)
        holder.textViewDescription.text = if (entry.url.toString() != "") entry.url else context.getString(R.string.password_list_fragment_no_url)
        holder.itemView.setOnClickListener {
            onItemClickListener?.onItemClick(holder.itemView, position);
        }

        holder.imageViewMenu.setOnClickListener {
            onItemMenuClickListener?.onItemMenuClick(holder.imageViewMenu, position);
        }
    }

    fun getItem(position: Int): Entry? {
        if (position < items.size) {
            return items[position]
        }
        else {
            return null
        }
    }

}

class ViewHolder(ItemView: View) : RecyclerView.ViewHolder(ItemView) {
    val imageViewImage: ImageView = itemView.findViewById(R.id.imageViewImage)
    val textViewTitle: TextView = itemView.findViewById(R.id.textViewTitle)
    val textViewDescription: TextView = itemView.findViewById(R.id.textViewDescription)
    val imageViewMenu: ImageView = itemView.findViewById(R.id.imageViewMenu)
}

interface EntryAdapterItemClickListener {
    fun onItemClick(view: View?, position: Int)
}

interface EntryAdapterItemMenuClickListener {
    fun onItemMenuClick(view: View?, position: Int)
}
