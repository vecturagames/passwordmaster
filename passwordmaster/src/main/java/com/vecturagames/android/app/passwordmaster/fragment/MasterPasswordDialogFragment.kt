package com.vecturagames.android.app.passwordmaster.fragment

import android.os.Bundle
import android.view.View
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import com.vecturagames.android.app.passwordmaster.R
import com.vecturagames.android.app.passwordmaster.preference.AppSettings

class MasterPasswordDialogFragment(defaultPassword: String = "", defaultPasswordRepeat: String = "", errorMessage: String = "") : DialogFragment() {

    private var mMasterPasswordDialogFragmentCallback: MasterPasswordDialogFragmentCallback? = null
    private var mDefaultPassword: String
    private var mDefaultPasswordRepeat: String
    private var mErrorMessage: String

    init {
        mDefaultPassword = defaultPassword
        mDefaultPasswordRepeat = defaultPasswordRepeat
        mErrorMessage = errorMessage
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): AlertDialog {
        val builder = AlertDialog.Builder(requireActivity())

        val layout: View?

        if (!AppSettings.passwordDbExists(requireActivity())) {
            layout = requireActivity().layoutInflater.inflate(R.layout.fragment_master_password_set, null)

            val editTextMasterPassword: EditText = layout.findViewById(R.id.editTextMasterPassword)
            val editTextMasterPasswordRepeat: EditText = layout.findViewById(R.id.editTextMasterPasswordRepeat)
            val textViewErrorMessage: TextView = layout.findViewById(R.id.textViewErrorMessage)

            if (!this.mDefaultPassword.equals("")) {
                editTextMasterPassword.setText(mDefaultPassword)
            }

            if (!this.mDefaultPasswordRepeat.equals("")) {
                editTextMasterPasswordRepeat.setText(mDefaultPasswordRepeat)
            }

            if (!this.mErrorMessage.equals("")) {
                textViewErrorMessage.setText(mErrorMessage)
            }
            else {
                textViewErrorMessage.visibility = View.GONE
            }

            builder.setMessage(R.string.master_password_fragment_info)

            builder.setPositiveButton(R.string.dialog_button_ok) { dialog, which ->
                if (mMasterPasswordDialogFragmentCallback != null) {
                    mMasterPasswordDialogFragmentCallback!!.onMasterPasswordPositiveCallback(editTextMasterPassword.text.toString(), editTextMasterPasswordRepeat.text.toString())
                }
            }

            builder.setNegativeButton(R.string.dialog_button_quit) { dialog, which ->
                requireActivity().finish()
            }
        }
        else {
            layout = requireActivity().layoutInflater.inflate(R.layout.fragment_master_password_enter, null)

            val editTextMasterPassword: EditText = layout.findViewById(R.id.editTextMasterPassword)
            val textViewErrorMessage: TextView = layout.findViewById(R.id.textViewErrorMessage)

            if (!this.mDefaultPassword.equals("")) {
                editTextMasterPassword.setText(mDefaultPassword)
            }

            if (!this.mErrorMessage.equals("")) {
                textViewErrorMessage.setText(mErrorMessage)
            }
            else {
                textViewErrorMessage.visibility = View.GONE
            }

            builder.setPositiveButton(R.string.dialog_button_ok) { dialog, which ->
                if (mMasterPasswordDialogFragmentCallback != null) {
                    mMasterPasswordDialogFragmentCallback!!.onMasterPasswordPositiveCallback(editTextMasterPassword.text.toString(), editTextMasterPassword.text.toString())
                }
            }

            builder.setNegativeButton(R.string.dialog_button_quit) { dialog, which ->
                requireActivity().finish()
            }
        }

        builder.setView(layout!!)
        builder.setTitle(R.string.master_password_fragment_title)
        builder.setCancelable(false)

        return builder.create()
    }

    fun setMasterPasswordDialogFragmentCallback(masterPasswordDialogFragmentCallback: MasterPasswordDialogFragmentCallback) {
        mMasterPasswordDialogFragmentCallback = masterPasswordDialogFragmentCallback
    }

    companion object {
        fun newInstance(defaultPassword: String, defaultPasswordRepeat: String, errorMessage: String): MasterPasswordDialogFragment {
            return MasterPasswordDialogFragment(defaultPassword, defaultPasswordRepeat, errorMessage)
        }
    }

}

interface MasterPasswordDialogFragmentCallback {
    fun onMasterPasswordPositiveCallback(password: String, passwordRepeat: String)
}
