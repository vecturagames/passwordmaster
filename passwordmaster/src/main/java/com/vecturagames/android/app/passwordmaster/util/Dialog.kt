package com.vecturagames.android.app.passwordmaster.util

import android.app.Activity
import android.content.Context

import androidx.appcompat.app.AlertDialog

import com.vecturagames.android.app.passwordmaster.R
import com.vecturagames.android.app.passwordmaster.enumeration.AppThemeType
import com.vecturagames.android.app.passwordmaster.preference.AppSettings

object Dialog {

    fun showAppThemeDialog(activity: Activity) {
        val originalAppTheme = AppSettings.instance.mAppTheme

        val appThemes = activity.resources.getStringArray(R.array.app_theme)

        val builder = AlertDialog.Builder(activity)
        builder.setTitle(activity.getString(R.string.entry_activity_menu_app_theme))
        builder.setSingleChoiceItems(appThemes, AppSettings.instance.mAppTheme.ordinal) { dialog, which ->
            AppSettings.instance.mAppTheme = AppThemeType.values()[which]
        }
        builder.setNegativeButton(R.string.dialog_button_cancel) { dialog, which ->
            AppSettings.instance.mAppTheme = originalAppTheme
        }
        builder.setPositiveButton(R.string.dialog_button_ok) { dialog, which ->
            if (originalAppTheme != AppSettings.instance.mAppTheme) {
                val builder = AlertDialog.Builder(activity)
                builder.setMessage(String.format(activity.getString(R.string.dialog_restart_theme_change), activity.getString(R.string.app_name)))
                builder.setCancelable(false)
                builder.setPositiveButton(R.string.dialog_button_ok) { dialog, id ->
                    Util.restartActivity(activity)
                }
                builder.show()
            }
        }
        builder.show()
    }

    fun showTipsDialog(context: Context) {
        showOkDialog(context, context.getString(R.string.entry_activity_menu_tips), context.getString(R.string.dialog_tips_text))
    }

    fun showPasswordStrengthDialog(context: Context) {
        showOkDialog(context, context.getString(R.string.entry_activity_menu_password_strength), String.format(context.getString(R.string.dialog_password_strength_text), context.getString(R.string.app_name), context.getString(R.string.app_name)))
    }

    fun showHelpDialog(context: Context) {
        showOkDialog(context, context.getString(R.string.entry_activity_menu_help), context.getString(R.string.dialog_help_text))
    }

    fun showOkDialog(context: Context, title: String?, message: String) {
        val builder = AlertDialog.Builder(context)
        if (title != null) {
            builder.setTitle(title)
        }
        builder.setMessage(message)
        builder.setPositiveButton(R.string.dialog_button_ok, null)
        builder.show()
    }

}