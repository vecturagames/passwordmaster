package com.vecturagames.android.app.passwordmaster.activity

import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.view.Gravity
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.PopupMenu
import androidx.appcompat.widget.Toolbar
import androidx.core.view.iterator
import androidx.drawerlayout.widget.DrawerLayout
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.navigation.NavigationView
import com.leinardi.android.speeddial.SpeedDialActionItem
import com.leinardi.android.speeddial.SpeedDialView
import com.vecturagames.android.app.passwordmaster.BuildConfig
import com.vecturagames.android.app.passwordmaster.R
import com.vecturagames.android.app.passwordmaster.adapter.EntryAdapter
import com.vecturagames.android.app.passwordmaster.adapter.EntryAdapterItemClickListener
import com.vecturagames.android.app.passwordmaster.adapter.EntryAdapterItemMenuClickListener
import com.vecturagames.android.app.passwordmaster.enumeration.AppThemeType
import com.vecturagames.android.app.passwordmaster.fragment.*
import com.vecturagames.android.app.passwordmaster.preference.AppSettings
import com.vecturagames.android.app.passwordmaster.util.Dialog
import com.vecturagames.android.app.passwordmaster.util.Util
import de.slackspace.openkeepass.domain.Entry
import de.slackspace.openkeepass.domain.EntryBuilder
import de.slackspace.openkeepass.domain.Group
import de.slackspace.openkeepass.domain.GroupBuilder
import de.slackspace.openkeepass.exception.KeePassDatabaseUnreadableException
import java.util.*

class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener, RecyclerViewContract, MasterPasswordDialogFragmentCallback, GroupDialogFragmentCallback, EntryAdapterItemClickListener, EntryAdapterItemMenuClickListener {

    private var mToolbar: Toolbar? = null
    private var mAppBarConfiguration: AppBarConfiguration? = null
    private var mDrawerLayout: DrawerLayout? = null
    private var mNavigationView: NavigationView? = null
    private var mNavigationController: NavController? = null
    private var mSpeedDialView: SpeedDialView? = null
    private var mRecyclerView: RecyclerView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
            splashScreen.setSplashScreenTheme(R.style.Theme_SplashScreen)
        }

        super.onCreate(savedInstanceState)

        setTheme(AppSettings.instance.appThemeId)
        setContentView(R.layout.activity_main)

        AppSettings.instance.mApplicationLaunchCount++

        mToolbar = findViewById(R.id.toolbar)
        setSupportActionBar(mToolbar)

        mSpeedDialView = findViewById(R.id.speedDialView)

        mSpeedDialView!!.addActionItem(SpeedDialActionItem.Builder(R.id.fab_add_group, R.drawable.group).setFabImageTintColor(Color.WHITE).setLabel(getString(R.string.main_activity_fab_add_group)).setTheme(AppSettings.instance.appThemeId).create())
        mSpeedDialView!!.addActionItem(SpeedDialActionItem.Builder(R.id.fad_add_entry, R.drawable.entry).setFabImageTintColor(Color.WHITE).setLabel(getString(R.string.main_activity_fab_add_entry)).setTheme(AppSettings.instance.appThemeId).create())

        mSpeedDialView!!.setOnActionSelectedListener(SpeedDialView.OnActionSelectedListener { actionItem ->
            when (actionItem.id) {
                R.id.fad_add_entry -> {
                    if (AppSettings.instance.mPasswordDb != null && AppSettings.instance.mPasswordDbCurrentGroupName != null) {
                        val group = AppSettings.instance.getPasswordDbCurrentGroup()

                        if (group != null) {
                            val entry = EntryBuilder().build()
                            group.entries.add(entry)
                            showEntry(entry)
                        }
                    }
                    mSpeedDialView!!.close()
                    return@OnActionSelectedListener true
                }
                R.id.fab_add_group -> {
                    if (AppSettings.instance.mPasswordDb != null) {
                        showGroupDialog("", 0)
                    }
                    mSpeedDialView!!.close()
                    return@OnActionSelectedListener true
                }
            }

            return@OnActionSelectedListener false
        })

        mDrawerLayout = findViewById(R.id.drawer_layout)
        mNavigationView = findViewById(R.id.navigation_view)
        mNavigationController = findNavController(R.id.navigation_host_fragment)

        mAppBarConfiguration = AppBarConfiguration(setOf(R.id.navigation_password_list), mDrawerLayout)

        setupActionBarWithNavController(mNavigationController!!, mAppBarConfiguration!!)
        mNavigationView!!.setupWithNavController(mNavigationController!!)
        mNavigationView!!.setNavigationItemSelectedListener(this)

        if (AppSettings.instance.mPasswordDb == null) {
            showMasterPasswordDialog("", "", "")
        }
    }

    override fun onDestroy() {
        AppSettings.destroyInstance()
        super.onDestroy()
    }

    override fun onPause() {
        AppSettings.instance.saveSettings()
        super.onPause()
    }

    override fun onBackPressed() {
        if (AppSettings.instance.mAutoClipboardClearing && Util.hasDataInClipboard(this@MainActivity)) {
            val builder = AlertDialog.Builder(this)
            builder.setMessage(String.format(getString(R.string.dialog_quit_clipboard), getString(R.string.app_name), getString(R.string.app_name)))
            builder.setPositiveButton(getString(R.string.dialog_button_yes)) { dialog, id ->
                Util.clearClipboard(this@MainActivity)
                finish()
            }
            builder.setNegativeButton(getString(R.string.dialog_button_no), null)
            builder.show()
        }
        else if (!AppSettings.instance.mRateDialogShowed && AppSettings.instance.mApplicationLaunchCount >= 5) {
            val builder = AlertDialog.Builder(this)
            builder.setMessage(String.format(getString(R.string.dialog_quit_rate), getString(R.string.app_name)))
            builder.setPositiveButton(getString(R.string.dialog_button_rate_it)) { dialog, id ->
                AppSettings.instance.mRateDialogShowed = true
                Util.openPlayStore(this@MainActivity, BuildConfig.APPLICATION_ID, 0)
                finish()
            }
            builder.setNegativeButton(getString(R.string.dialog_button_no_thanks)) { dialog, id ->
                AppSettings.instance.mRateDialogShowed = true
                finish()
            }
            builder.show()
        }
        else {
            super.onBackPressed()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.main, menu)
        menu.findItem(R.id.auto_clipboard_clearing)?.setChecked(AppSettings.instance.mAutoClipboardClearing)
        return true
    }

    override fun onSupportNavigateUp(): Boolean {
        val navigationController = findNavController(R.id.navigation_host_fragment)
        return navigationController.navigateUp(mAppBarConfiguration!!) || super.onSupportNavigateUp()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.auto_clipboard_clearing -> {
                AppSettings.instance.mAutoClipboardClearing = !AppSettings.instance.mAutoClipboardClearing
                item.setChecked(AppSettings.instance.mAutoClipboardClearing)
                return true
            }

            R.id.menu_app_theme -> {
                Dialog.showAppThemeDialog(this)
                return true
            }

            R.id.menu_tips -> {
                Dialog.showTipsDialog(this)
                return true
            }

            R.id.menu_about -> {
                val intent = Intent(this@MainActivity, AboutActivity::class.java)
                startActivity(intent)
                return true
            }

            else -> return super.onOptionsItemSelected(item)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, intent: Intent?) {
        when(requestCode) {
            EntryActivity.REQUEST_CODE_SHOW_ENTRY -> {
                updateCurrentGroupEntries()
            }
        }

        super.onActivityResult(requestCode, resultCode, intent)
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        if (AppSettings.instance.mPasswordDb != null) {
            for (it in mNavigationView!!.menu.iterator()) {
                it.setChecked(false)
            }

            AppSettings.instance.mPasswordDbCurrentGroupName = item.title.toString()

            mToolbar!!.title = item.title
            item.setChecked(true)

            updateCurrentGroupEntries()
        }

        mDrawerLayout!!.closeDrawers()

        return true
    }

    override fun setRecyclerView(recyclerView: RecyclerView) {
        mRecyclerView = recyclerView
    }

    override fun onMasterPasswordPositiveCallback(password: String, passwordRepeat: String) {
        if (password.isEmpty()) {
            showMasterPasswordDialog(password, passwordRepeat, getString(R.string.dialog_empty_password))
        }
        else if (!password.equals(passwordRepeat)) {
            showMasterPasswordDialog(password, passwordRepeat, getString(R.string.dialog_incorrect_repeated_password))
        }
        else if (password.length < 16) {
            showMasterPasswordDialog(password, passwordRepeat, getString(R.string.dialog_minimum_password_length_does_not_met))
        }
        else {
            if (!AppSettings.passwordDbExists(this)) {
                AppSettings.createPasswordDb(this, password)
            }

            try {
                val passwordDb = AppSettings.instance.unlockPasswordDb(this, password)

                if (passwordDb != null) {
                    updateNavigationViewItems()

                    onNavigationItemSelected(mNavigationView!!.getMenu().getItem(0));
                    mDrawerLayout!!.openDrawer(Gravity.LEFT)

                    mSpeedDialView!!.show()
                }
                else {
                    showMasterPasswordDialog(password, passwordRepeat, getString(R.string.dialog_incorrect_password))
                }
            }
            catch (exception: KeePassDatabaseUnreadableException) {
                showMasterPasswordDialog(password, passwordRepeat, getString(R.string.dialog_incorrect_password))
            }
        }
    }

    override fun onGroupPositiveCallback(groupName: String, imageResIdx: Int) {
        if (!groupName.isEmpty() && AppSettings.instance.mPasswordDb != null) {
            if (AppSettings.instance.mPasswordDb!!.topGroups.find { it.name.lowercase(Locale.ENGLISH) == groupName.lowercase(Locale.ENGLISH) } == null) {
                val group: Group = GroupBuilder(groupName).iconId(imageResIdx).build()
                AppSettings.instance.mPasswordDb!!.topGroups.add(group)
                AppSettings.instance.mPasswordDb!!.topGroups.sortBy { it.name.lowercase(Locale.ENGLISH) }
                AppSettings.instance.writePasswordDb(this)
                updateNavigationViewItems()
            }
            else {
                Dialog.showOkDialog(this, getString(R.string.dialog_error), getString(R.string.dialog_group_name_already_exists))
            }
        }
    }

    override fun onItemClick(view: View?, position: Int) {
        if (mRecyclerView?.adapter is EntryAdapter) {
            val entry = (mRecyclerView?.adapter as EntryAdapter).getItem(position)

            if (entry != null) {
                showEntry(entry)
            }
        }
    }

    override fun onItemMenuClick(view: View?, position: Int) {
        if (mRecyclerView?.adapter is EntryAdapter && view != null) {
            val entry = (mRecyclerView?.adapter as EntryAdapter).getItem(position)

            if (entry != null) {
                val popupMenu = PopupMenu(this, view)
                popupMenu.inflate(R.menu.entry_item)

                popupMenu.setOnMenuItemClickListener {
                    if (it.itemId == R.id.menu_open_url) {
                        if (entry.url != null && entry.url != "") {
                            Util.openInternetBrowser(this, entry.url, 0)
                        }
                    }
                    else if (it.itemId == R.id.menu_copy_password) {
                        if (entry.password != null && entry.password != "") {
                            Util.copyToClipboard(this, entry.password)
                        }
                    }
                    else if (it.itemId == R.id.menu_edit) {
                        showEntry(entry)
                    }
                    else if (it.itemId == R.id.menu_delete) {
                        val builder = AlertDialog.Builder(this)
                        builder.setMessage(getString(R.string.dialog_delete_entry_confirmation))
                        builder.setPositiveButton(getString(R.string.dialog_button_yes)) { dialog, id ->
                            val group = AppSettings.instance.getPasswordDbCurrentGroup()
                            if (group != null) {
                                group.entries.remove(entry)
                                updateCurrentGroupEntries()
                            }
                        }
                        builder.setNegativeButton(getString(R.string.dialog_button_no), null)
                        builder.show()
                    }
                    true
                }

                popupMenu.show()
            }
        }
    }

    private fun showMasterPasswordDialog(password: String, passwordRepeat: String, errorMessage: String) {
        val masterPasswordDialogFragment = MasterPasswordDialogFragment.newInstance(password, passwordRepeat, errorMessage)
        masterPasswordDialogFragment.setMasterPasswordDialogFragmentCallback(this)
        masterPasswordDialogFragment.setCancelable(false)
        masterPasswordDialogFragment.show(supportFragmentManager, FRAGMENT_MASTER_PASSWORD_TAG)
    }

    private fun showGroupDialog(groupName: String, imageResIdx: Int) {
        val groupDialogFragment = GroupDialogFragment.newInstance(groupName, imageResIdx)
        groupDialogFragment.setAddGroupDialogFragmentCallback(this)
        groupDialogFragment.show(supportFragmentManager, FRAGMENT_GROUP_TAG)
    }

    private fun showEntry(entry: Entry) {
        val intent = Intent(this@MainActivity, EntryActivity::class.java)
        intent.putExtra(EntryActivity.BUNDLE_TAG_GROUP_NAME, AppSettings.instance.mPasswordDbCurrentGroupName)
        intent.putExtra(EntryActivity.BUNDLE_TAG_ENTRY_UUID, entry.uuid.toString())
        startActivityForResult(intent, EntryActivity.REQUEST_CODE_SHOW_ENTRY)
    }

    private fun updateNavigationViewItems() {
        if (AppSettings.instance.mPasswordDb != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                mNavigationView!!.itemTextColor = if (AppSettings.instance.mAppTheme == AppThemeType.LIGHT) getColorStateList(R.color.selector_navigation_text_lighttheme) else getColorStateList(R.color.selector_navigation_text_darktheme)
                mNavigationView!!.foregroundTintList = if (AppSettings.instance.mAppTheme == AppThemeType.LIGHT) getColorStateList(R.color.selector_navigation_text_lighttheme) else getColorStateList(R.color.selector_navigation_text_darktheme)
                mNavigationView!!.itemIconTintList = if (AppSettings.instance.mAppTheme == AppThemeType.LIGHT) getColorStateList(R.color.selector_navigation_text_lighttheme) else getColorStateList(R.color.selector_navigation_text_darktheme)
            }
            mNavigationView!!.setItemBackgroundResource(if (AppSettings.instance.mAppTheme == AppThemeType.LIGHT) R.drawable.selector_navigation_background_lighttheme else R.drawable.selector_navigation_background_darktheme)
            mNavigationView!!.menu.clear()
            var i = 0

            for (group in AppSettings.instance.mPasswordDb!!.topGroups) {
                if (group.name != null && group.iconId < AppSettings.DEFAULT_ICONS.size) {
                    mNavigationView!!.menu.add(R.id.navigationPasswordGroups, i, i, group.name).setIcon(AppSettings.DEFAULT_ICONS[group.iconId]).setCheckable(true).setChecked(false)
                    i++
                }
            }
        }
    }

    private fun updateCurrentGroupEntries() {
        val group = AppSettings.instance.getPasswordDbCurrentGroup()
        if (group != null && mRecyclerView != null) {
            mRecyclerView!!.adapter = EntryAdapter(group.entries, this, this, this)
        }
    }

    companion object {
        private val FRAGMENT_MASTER_PASSWORD_TAG = "fragment_master_password_set"
        private val FRAGMENT_GROUP_TAG = "fragment_group"
    }

}
